package com.vsii.tsc.guru.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utility {
	
	static FileInputStream file;
	static XSSFWorkbook workbook;
	static XSSFSheet sheet;
	static XSSFRow row;
	static Cell cell;
	
	//Use for getTable() method
	static String testData[][];
	
	//Use for writeTestResults() method
	static ArrayList<String> valueList;
	
//--------------------------------------------------Methods--------------------------------------------------------------------
/*
 * Read from configuration file
 */
	public static Properties readConfig() throws IOException {
		// Create new properties variable
		Properties p = new Properties();
		// Read object properties file
		InputStream stream = new FileInputStream("./properties/config.properties");
		// Load input stream file
		p.load(stream);
		return p;
	}
//-----------------------------------------------------------------------------------------------------------------------------
/*
 * Read data from table in excel file
 */
	@SuppressWarnings("resource")
	public static String[][] getTable(String filePath, String sheetName, String tableName) {
		String tagName = null;
		List<Cell> tagList = new ArrayList<Cell>();
		int startRow = 0;
		int startCol = 0;
		int endRow = 0;
		int endCol = 0;

		// Open file
		try {
			file = new FileInputStream(filePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Define workbook
		try {
			workbook = new XSSFWorkbook(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Define sheet
		sheet = workbook.getSheet(sheetName);

		// Iterate through each row
		Iterator<Row> rowIterator = sheet.iterator();
		while (rowIterator.hasNext()) {
			row = (XSSFRow) rowIterator.next();
			Iterator<Cell> cellIterator = row.iterator();
			while (cellIterator.hasNext()) {
				cell = cellIterator.next();
				// Check the cell type and format
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					tagName = cell.getStringCellValue();
					// case Cell.CELL_TYPE_NUMERIC:
					// = String.valueOf(cell.getNumericCellValue());
				}

				if (tagName.equalsIgnoreCase(tableName)) {
					tagList.add(cell);
				}
			}
		}
		// Identify table
		if (tagList.size() == 2) {
			startRow = tagList.get(0).getRowIndex() + 1;
			System.out.println(startRow);
			startCol = tagList.get(0).getColumnIndex() + 1;
			System.out.println(startCol);
			endRow = tagList.get(1).getRowIndex() - 1;
			System.out.println(endRow);
			endCol = tagList.get(1).getColumnIndex() - 1;
			System.out.println(endCol);
		} else
			System.out.println("Wrong table");

		testData = new String[endRow - startRow + 1][endCol - startCol + 1];

		int x = 0;
		for (int i = startRow; i <= endRow; i++, x++) {
			int y = 0;
			for (int j = startCol; j <= endCol; j++, y++) {
				row = sheet.getRow(i);
				testData[x][y] = row.getCell(j).toString();
				System.out.println(testData[x][y]);
			}
		}
		return testData;
	}

//------------------------------------------------------------------------------------------------------------------------------
/*
 * Get value list on the column
 */
	@SuppressWarnings("resource")
	public static List<String> loadList(String filePath,String sheetName,int tcIDCol) {

		List<String> valueList = new ArrayList<String>();
		int columnWanted;

		//List<ActionModel> actionList = new ArrayList<ActionModel>();

		// Open file
		try {
			file = new FileInputStream(new File(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		// Define workbook
		try {
			workbook = new XSSFWorkbook(file);
			System.out.println("successfully");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("unsuccessfully");
		}
		// define sheet
		sheet = workbook.getSheet(sheetName);

		// Col is Test Case ID's column
		columnWanted = tcIDCol;
		for (int r = 0;r<sheet.getLastRowNum(); r++) {
		  Cell c = null;	
		  Row row1 = sheet.getRow(r);
		  c = row1.getCell(columnWanted);
		   if (c == null || c.getCellType() == Cell.CELL_TYPE_BLANK) {
		      // Nothing in the cell in this row, skip it
		   } else valueList.add(c.getStringCellValue());
		}
		// for (String s:caseNameList){
		// System.out.println(s+",");
		// }

		// Get table for all Suites
		
		return valueList;
	
	}
	
//-------------------------------------------------------------------------------------------------------------------------------
/*
 * Open excel file
 */
	public static void openExcelFile(String filePath, String sheetName, int tcIDCol) throws IOException{
		file = new FileInputStream(new File(filePath));
        workbook = new XSSFWorkbook(file);
        //list of test case name
         valueList= (ArrayList<String>) Utility.loadList(filePath, sheetName, tcIDCol);
        //create a new work sheet
        sheet = workbook.getSheet("DemoTestcase");
		
	}
//-------------------------------------------------------------------------------------------------------------------------------
/*
 * Write results to excel
 */
	public static void writeTestResults(String caseName, int writtenCol, String testResult) throws IOException{
			
		for (String cellContent:valueList){
				//If find the test case ID equal to the name of test method, fill the test results.
			if(cellContent.equalsIgnoreCase(caseName)){
					System.out.println(cellContent);
				int rowNum;
				int colNum;
				int newColNum;
				for (Row row : sheet) {
					for (Cell cell : row) {
						if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
							if (cell.getRichStringCellValue().getString().trim().equals(cellContent)) {
		                	// cell.setCellValue("Passed");
								rowNum = row.getRowNum();
								colNum = cell.getColumnIndex();
								newColNum = colNum+writtenCol;
								sheet.getRow(rowNum).getCell(newColNum).setCellValue(testResult);	
		                	
							}
						}
					}
				}
		    }  
		}
		 
	}
//-----------------------------------------------------------------------------------------------------------------------------
/*
 * Create excel file
 */
public static void createExcelFile(String fileName){
	try {
        FileOutputStream out =new FileOutputStream(new File(fileName));
        workbook.write(out);
        out.close();
        System.out.println("Excel written successfully..");

    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
}
	
	
}
