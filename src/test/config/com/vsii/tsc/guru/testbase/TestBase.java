package com.vsii.tsc.guru.testbase;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import com.vsii.tsc.guru.report.Report;
import com.vsii.tsc.guru.utility.Utility;

public class TestBase {
	protected WebDriver driver;
	protected Properties p;
	protected ExtentReports extent;
	protected ExtentTest test;
	
	@BeforeSuite
	public void beforeSuite() throws IOException {
		
		// Read config file
		p = Utility.readConfig();
		switch (p.getProperty("browserName")) {
		// Open Firefox browser
		case ("Firefox"):
			driver = new FirefoxDriver();
			break;
		// Open Chrome browser
		case ("Chrome"):
			System.setProperty(p.getProperty("chromeDriver"), p.getProperty("chromeDriverPath"));
			driver = new ChromeDriver();
			break;
		// Open Internet Explorer browser
		case ("IE"):
			System.setProperty(p.getProperty("ieDriver"), p.getProperty("ieDriverPath"));
			driver = new InternetExplorerDriver();
			break;
		// Open Internet Explorer browser
		case ("Safari"):
			driver = new SafariDriver();
			break;
		default:
			break;
		}
		// Open base URL
		driver.get(p.getProperty("baseUrl"));
		driver.manage().window().maximize();
		extent = Report.Instance();
	}

	@AfterSuite
	public void afterSuite() {
		//extent.endTest(test);
		
	}
}
