package com.vsii.tsc.guru.testcase;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.vsii.tsc.guru.data.TestData;
import com.vsii.tsc.guru.pages.method.EditCustomerPageMethod;
import com.vsii.tsc.guru.pages.method.LoginPageMethod;
import com.vsii.tsc.guru.pages.method.NewCustomerPageMethod;
import com.vsii.tsc.guru.report.Report;
import com.vsii.tsc.guru.testbase.TestBase;

public class EditCustomer extends TestBase {
	EditCustomerPageMethod objEditCust;
	LoginPageMethod objLogin;
	
	@BeforeMethod
	public void before() throws IOException{
		objEditCust = new EditCustomerPageMethod(driver);
		objLogin = new LoginPageMethod(driver);
		//extent = Report.Instance();
	}

	@Test(priority = 0, dataProvider = "dpLogin_success", dataProviderClass = TestData.class)
	public void verify_Login(String username, String password, String message) throws Exception {
		
		//test = extent.startTest("Login for Verify Edit Customer");
		objLogin.loginToManagerPage(username, password);
	}

	//@Test(priority = 1, description = "Edit Customer")
	public void verify_Edit_Cutomer_Form_Display() throws IOException {
		objEditCust.clickEditCustomer();
		objEditCust.enterCustomerID("67743");
		objEditCust.clickSubmit();
		String custNameLabel = objEditCust.getCustomerNameLabel();
		test.log(LogStatus.INFO, "<b>VERIFY EDIT CUSTOMER</b>");
		if (custNameLabel.contains("Customer Name")) {
			test.log(LogStatus.PASS, "Go to Edit customer screen successfully");
		} else {
			test.log(LogStatus.FAIL, "Wrong customer ID");
		}
		test.log(LogStatus.INFO, test.addScreenCapture(Report.CaptureScreen(driver, "EditCustomer")));
	}

	//@Test(priority = 2, description = "Check for disabled textbox")
	public void check_Disabled_Textbox() throws IOException {
		boolean isDisabled = objEditCust.checkTextBoxDisabled();
		test.log(LogStatus.INFO, "<b>VERIFY EDIT CUSTOMER</b>");
		if (isDisabled) {
			test.log(LogStatus.PASS, "Customer Name, Gender, Date of Birth textbox are disabled");
		} else {
			test.log(LogStatus.FAIL, "Customer Name, Gender, Date of Birth textbox are disabled");
		}
		test.log(LogStatus.INFO, test.addScreenCapture(Report.CaptureScreen(driver, "Check disabled")));
	}

	@Test(priority = 3, description = "Update customer information", dataProvider = "dpEditCustomer", dataProviderClass = TestData.class)
	public void TC_03(String customerID, String address, String city, String state, String pin, String mobileNo, String email, String message) throws IOException {
		objEditCust.clickEditCustomer();
		objEditCust.enterCustomerID(customerID);
		objEditCust.clickSubmit();
		objEditCust.editCustomerInfo(address, city, state, pin, mobileNo, email);
		
		//Write log
		test = extent.startTest("Verify Edit Customer");
		test.log(LogStatus.INFO, "<b>VERIFY EDIT CUSTOMER</b>");
		String updateSuccess = objEditCust.getUpdateSuccessInfo();
		
		//Verify expected results
		if (updateSuccess.contains(message)) {
			test.log(LogStatus.PASS, "Update user information successfully");
		} 
		else {
			test.log(LogStatus.FAIL, "Update user information not successfully");
			Assert.assertTrue(updateSuccess.contains(message));
		}
		test.log(LogStatus.INFO, test.addScreenCapture(Report.CaptureScreen(driver, "TC_12")));
		extent.endTest(test);
	}
	
	@AfterMethod
	public void after(){
		objEditCust = null;
		objLogin = null;
	
	}
}
