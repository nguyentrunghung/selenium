package com.vsii.tsc.guru.testcase;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.vsii.tsc.guru.data.TestData;
import com.vsii.tsc.guru.pages.method.LoginPageMethod;
import com.vsii.tsc.guru.report.Report;
import com.vsii.tsc.guru.testbase.TestBase;
import com.vsii.tsc.guru.utility.CommonOperations;
import com.vsii.tsc.guru.utility.Utility;

public class Login extends TestBase {
	LoginPageMethod objLogin;
	Report report;
	
	
	@BeforeClass
	public void beforeClass() throws NumberFormatException, IOException{
		p = Utility.readConfig();
		Utility.openExcelFile(p.getProperty("tcFile"),p.getProperty("LoginModule"),Integer.parseInt(p.getProperty("tcID")));
	}
	
	@AfterClass
	public void afterClass(){
		extent.flush();
		extent.close();
		driver.quit();
		//Create test case file after filling test result
		Utility.createExcelFile(p.getProperty("tcFileOut"));
	}
	

	@BeforeMethod
	public void beforeMethod() throws IOException {
		objLogin = new LoginPageMethod(driver);	
		report = new Report();
	}

//	//@Test(priority = 0, description = "verify_Title_Login_Page")
//	public void TC01 () throws IOException {
//		// Verify Login page title
//		String loginPageTitle = objLogin.getLoginTitle();
//		// Assert.assertTrue(loginPageTitle.contains("Guru99 Bank"));
//		test.log(LogStatus.INFO, "<b>TC01_VERIFY TITLE OF LOGIN PAGE</b>");
//		Assert.assertTrue(loginPageTitle.contains("Guru99 Bank")) ;
//		test.log(LogStatus.INFO, test.addScreenCapture(Report.CaptureScreen(driver, "TC01")));
//	}
//
//	
//	//@Test(priority = 1, description = "verify_Alert_When_UserID_Textbox_Is_EmptyTC02")
//	public void TC_02() throws Exception {
//		objLogin.clickUserID();
//		objLogin.clickPassword();
//		String userIdAlert = objLogin.getUserIdAlert();
//		Assert.assertTrue(userIdAlert.contains("User-ID must not be blank"));
//		test.log(LogStatus.INFO, "<b>TC02_VERIFY ALERT WHEN USER ID TEXTBOX IS EMPTY</b>");
//		test.log(LogStatus.PASS, "User-ID must not be blank text displays");
//		test.log(LogStatus.INFO, test.addScreenCapture(Report.CaptureScreen(driver, "TC02")));
//	}
//
//	//@Test(priority = 2, description = "verify_Alert_When_Password_Textbox_Is_Empty")
//	public void TC_03() throws Exception {
//		objLogin.clickPassword();
//		objLogin.clickUserID();
//		String passwordAlert = objLogin.getPasswordAlert();
//		Assert.assertTrue(passwordAlert.contains("Password must not be blank"));
//		
//		test = extent.startTest("Test");
//		
//		test.log(LogStatus.INFO, "<b>TC03_VERIFY ALERT WHEN PASSWORD TEXTBOX IS EMPTY</b>");
//		test.log(LogStatus.PASS, "Password must not be blank text displays");
//		test.log(LogStatus.INFO, test.addScreenCapture(Report.CaptureScreen(driver, "TC_03")));
//	}
//
//	@Test(priority = 3, description = "verify_Alert_When_Both_UserID_And_Password_Is_Empty")
//	public void TC_04() throws Exception {
//		objLogin.clickLogin();
//		String txtPopup = objLogin.getPopupText();
//		Assert.assertTrue(txtPopup.contains("User or Password is not valid"));
//		
//		test = extent.startTest("Test Test2");
//		
//		test.log(LogStatus.INFO, "<b>TC04_VERIFY ALERT WHEN BOTH USER ID AND PASSWORD IS EMPTY</b>");
//		test.log(LogStatus.PASS, "User or Password is not valid alert displays");
//		test.log(LogStatus.INFO, test.addScreenCapture(
//				Report.captureScreenShotPopUp(p.getProperty("imagePath")+"TC_04")));
//		objLogin.closePopup();
//	}

	@Test(priority = 4, description = "verify Login", dataProvider = "dpLogin", dataProviderClass = TestData.class)
	public void TC_01(String username, String password, String message) throws Exception {	
		
		if(objLogin==null)
		objLogin = new LoginPageMethod(driver);
		if(report==null)
			report = new Report();
		
		//perform login
		objLogin.loginToManagerPage(username, password);
		//Boolean alert = CommonOperations.isAlertPresent(driver);
	
		//Write to log
		test = extent.startTest("Test Login");
		
		//input invalid account, having popup. If not, login successfully
		if(CommonOperations.isAlertPresent(driver)){
			String txtPopup = objLogin.getPopupText();
			
			if(txtPopup.contains("User or Password is not valid")){
				test.log(LogStatus.INFO, "<b>VERIFY ALERT WHEN ACCOUNT IS INVALID</b>");
				test.log(LogStatus.PASS, "User or Password is not valid alert displays");
				objLogin.closePopup();
			}
			else{
				test.log(LogStatus.FAIL, "User or Password is not valid alert displays");
			}
				String directoryName="D:/MyJobs/RemarkMedia/Build_Project_for_TSC/seleniumDemo/report/screenshot";
				File theDir = new File(directoryName);
				if(!theDir.exists())
				{
					theDir.mkdir();
				}
				String img = report.captureScreenShotPopUp(directoryName+"/TC_01_Invalid");
			
				test.log(LogStatus.INFO, test.addScreenCapture(img));
		
		}
		else {		
				test.log(LogStatus.INFO, "<b>VERIFY LOGIN WHEN ACCOUNT IS VALID</b>");
				//get message when user login successfully
				String managerID; managerID = objLogin.getManagerIDInManagerPage();
			
				if(managerID.contains(message)){
				
				test.log(LogStatus.PASS, "User logs in successfully");			
			}
				else
			{
				test.log(LogStatus.FAIL, "User logs in not successfully");
				Assert.assertTrue(managerID.contains(message));
			}
			test.log(LogStatus.INFO, test.addScreenCapture(Report.CaptureScreen(driver, "TC_01_Valid")));
		
		}
		
		
	}
	
	//@Test(priority = 7, dataProvider = "dpLogin_success", dataProviderClass = TestData.class,description="Login successfully" )
//	public void TC_08(String username, String password, String message) throws Exception {
//		
//		objLogin.loginToManagerPage(username, password);
//		String managerID = objLogin.getManagerIDInManagerPage();
//		test.log(LogStatus.INFO, "<b>TC09_VERIFY LOGIN FUNCTION</b>");
//		if(managerID.contains(message)){
//			test.log(LogStatus.PASS, "User logs in successfully");
//		}
//		else
//		{
//			test.log(LogStatus.FAIL, "User logs in not successfully");
//			Assert.assertTrue(managerID.contains(message));
//		}
//		test.log(LogStatus.INFO, test.addScreenCapture(Report.CaptureScreen(driver, "TC08")));
//	}
//	
//	//@Test(priority = 6, description = "verify_Reset_Button")
//		public void TC07() throws Exception {
//			objLogin.setUserID("trunghung");
//			objLogin.setPassword("123456");
//			objLogin.clickReset();
//			Assert.assertEquals(objLogin.getUserID(), "");
//			Assert.assertEquals(objLogin.getPassword(), "");
//			test.log(LogStatus.INFO, "<b>TC08_VERIFY RESET BUTTON</b>");
//			test.log(LogStatus.PASS, "User and Password are reset");
//			test.log(LogStatus.INFO, test.addScreenCapture(Report.CaptureScreen(driver, "TC07")));
//		}
		
	@AfterMethod
	public void afterMethod() {
		objLogin = null;
		
		extent.endTest(test);
	}
	
	
}
